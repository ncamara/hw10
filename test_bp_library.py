#!/usr/bin/python3
import unittest
from best_picture_library import BestPictureLibrary

class TestBPLibrary(unittest.TestCase):
	bpl = BestPictureLibrary()
	bpl.load_movies('data.json')

	def test_get_year(self):
		year_movies = self.bpl.get_year(2015)

		for movie in year_movies:
			self.assertEqual(movie['year'], 2015)

	def test_get_winners(self):
		winner_movies = self.bpl.get_winners()

		for movie in winner_movies:
			self.assertEqual(movie['winner'], 'TRUE')

	def test_add_movie(self):
		year = 2020
		winner = 'TRUE'
		title = 'Mank'

		self.bpl.add_movie(year, winner, title)
		movie = self.bpl.movies[-1]

		self.assertEqual(movie['year'], year)
		self.assertEqual(movie['winner'], winner)
		self.assertEqual(movie['title'], title)

	def test_edit_movie(self):
		year = 2020
		winner = 'TRUE'
		title = 'Mank'

		self.bpl.add_movie(year, winner, title)
		movie = self.bpl.movies[-1]

		self.assertEqual(movie['year'], year)
		self.assertEqual(movie['winner'], winner)
		self.assertEqual(movie['title'], title)

		id = movie['id']

		self.bpl.edit_movie(id, 2020, winner='TRUE', title='Fake Movie')

		self.assertEqual(movie['year'], 2020)
		self.assertEqual(movie['winner'], 'TRUE')
		self.assertEqual(movie['title'], 'Fake Movie')

	def test_delete_movie(self):
		self.bpl.add_movie(2020, 'FALSE', 'Vincent Fake Movie')
		movie = self.bpl.movies[-1]

		id = movie['id']
		title = movie['title']

		self.bpl.delete_movie(id)

		movie = self.bpl.movies[-1]

		self.assertNotEqual(movie['id'], id)
		self.assertNotEqual(movie['title'], title)

	def test_delete_all(self):
		self.bpl.add_movie(2021, 'FALSE', 'New 2021 Fake Movie')
		self.bpl.add_movie(2021, 'FALSE', 'Other Fake Movie')

		self.bpl.delete_all()

		lastMovie = self.bpl.movies[-1]

		self.assertEqual(lastMovie['year'], 2019)

if __name__ == '__main__':
	unittest.main()
