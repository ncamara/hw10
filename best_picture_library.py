#!/usr/bin/python3

import json

class BestPictureLibrary:

	def __init__(self):
		self.movies = dict()
		self.id_counter = 1

	def find_id(self, id):
		found = False
		for movie in self.movies:
			if id in movie.values():
				found = True
		return found
	
	def load_movies(self, bp_file):
		with open(bp_file) as file:
			self.movies = json.load(file)


		for movie in self.movies:
			movie['title'] = movie['entity']
			movie['id'] = self.id_counter
			self.id_counter += 1
			del movie['entity']

	def get_all_movies(self):
		return self.movies
	
	def get_year(self, year):
		year_movies = list()
		for movie in self.movies:
			if movie['year'] == year:
				year_movies.append(movie)
		return year_movies

	def get_winners(self):
		winners_list = list()
		for movie in self.movies:
			if movie['winner'] == 'TRUE':
				winners_list.append(movie)
		return winners_list
	
	def get_year_winners(self, year):
		winners_list = list()
		for movie in self.movies:
			if movie['winner'] == 'TRUE' and movie['year'] == year:
				winners_list.append(movie)

		return winners_list

	def edit_movie(self, id, year, winner=None, title=None):
		for movie in self.movies:
			if id in movie.values():
				if movie['year'] < 2020:
					print('Cannot edit movies before 2020')
					return False
				else:
					if winner is not None:
						movie['winner'] = winner
					if title is not None:
						movie['title'] = title
				return True

		return False

	def add_movie(self, year, winner, title):
		if year < 2020:
			return False

		new_movie = dict()
		new_id = self.id_counter
		self.id_counter = self.id_counter + 1
		new_movie['year'] = year
		new_movie['category'] = 'BEST PICTURE'
		new_movie['winner'] = winner
		new_movie['title'] = title
		new_movie['id'] = new_id
		self.movies.append(new_movie)
		return True

	def delete_movie(self, id):
		for movie in self.movies:
			if movie['id'] == int(id):
				if movie['year'] >= 2020:
					index = self.movies.index(movie)
					removed_movie = self.movies.pop(index)
					return True
				else:
					return False

		return False

	def delete_all(self):
		for movie in reversed(self.movies):
			if movie['year'] >= 2020:
				index = self.movies.index(movie)
				removed_movie = self.movies.pop(index)

