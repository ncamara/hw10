#!/usr/bin/python3
import requests
import json
import unittest

class TestBPServer(unittest.TestCase):

	URL = 'http://localhost:51059/bp/'
	RESET_URL = 'http://localhost:51059/reset/'

	def reset_data(self):
		m = {}
		r = requests.put(self.RESET_URL, json.dumps(m))

	def test_get_all(self):
		r = requests.get(self.URL)
		resp = json.loads(r.content.decode())

		testmovie = {}
		movies = resp['movies']

		for movie in movies:
			if movie['id'] == 1:
				testmovie = movie

		self.assertEqual(testmovie['title'], 'The Racket')
		self.assertEqual(testmovie['year'], 1927)

	
	def test_get_year(self):

		r = requests.get(self.URL + 'year/2015')
		resp = json.loads(r.content.decode())

		movies = resp['movies']
		for movie in movies:
			self.assertEqual(movie['year'], 2015)

	
	def test_get_winners(self):
		r = requests.get(self.URL + 'winners')
		resp = json.loads(r.content.decode())

		movies = resp['movies']
		for movie in movies:
			self.assertEqual(movie['winner'], 'TRUE')

	
	def test_get_winners_year(self):
		r = requests.get(self.URL + 'winners/2015')
		resp = json.loads(r.content.decode())

		movies = resp['movies']
		movie = movies[0]

		self.assertEqual(movie['title'], 'Spotlight')
		self.assertEqual(movie['year'], 2015)
		self.assertEqual(movie['winner'], 'TRUE')

	
	def test_post_movie(self):
		self.reset_data()
		payload = {'year' : 2020, 'title' : 'Mank', 'winner' : 'FALSE'}
		r = requests.post('http://localhost:51059/bp/', data=json.dumps(payload))
		
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')
		
		r = requests.get(self.URL + 'year/2020')
		resp = json.loads(r.content.decode())
		movies = resp['movies']
		movie = movies[0]
		self.assertEqual(movie['title'], 'Mank')
		self.assertEqual(movie['winner'], 'FALSE')

	
	def test_put_movie(self):
		payload = {'id' : 564, 'year' : 2020, 'title' : 'Minari'}

		r = requests.put('http://localhost:51059/bp/', data=json.dumps(payload))
		
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.URL + 'year/2020')
		resp = json.loads(r.content.decode())
		movies = resp['movies']
		movie = movies[0]
		self.assertEqual(movie['title'], 'Minari')
		self.assertEqual(movie['id'], 564)

	def test_delete_movie(self):
		self.reset_data()
		payload = {'year' : 2020, 'title' : 'Minari', 'winner' : 'FALSE'}
		r = requests.post(self.URL, data=json.dumps(payload))


		r = requests.delete(self.URL + '564', data={})
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.URL)
		resp = json.loads(r.content.decode())
		movies = resp['movies']
		movie = movies[-1]
		self.assertNotEqual(movie['id'], 564)
		self.assertNotEqual(movie['title'], 'Minari')



	def test_delete_all(self):
		self.reset_data()
		payload = {'year' : 2020, 'title' : 'Mank', 'winner' : 'FALSE'}
		r = requests.post('http://localhost:51059/bp/', data=json.dumps(payload))
		payload = {'year' : 2020, 'title' : 'Minari', 'winner' : 'FALSE'}
		r = requests.post('http://localhost:51059/bp/', data=json.dumps(payload))
		r = requests.delete(self.URL, data={})
		resp = json.loads(r.content.decode())
		self.assertEqual(resp['result'], 'success')

		r = requests.get(self.URL)
		resp = json.loads(r.content.decode())
		movies = resp['movies']
		movie = movies[-1]
		self.assertNotEqual(movie['id'], 564)
		self.assertNotEqual(movie['title'], 'Minari')
		
if __name__ == '__main__':
	unittest.main()
