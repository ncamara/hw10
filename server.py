#!/usr/bin/python3

import routes
import cherrypy
from best_picture_controller import BestPictureController
from best_picture_library import BestPictureLibrary

def start_service():
	bpl = BestPictureLibrary()
	bpc = BestPictureController(bpl=bpl)

	dispatcher = cherrypy.dispatch.RoutesDispatcher()


	# Dispatcher connection
	dispatcher.connect('get_all', '/bp/', controller=bpc, action='GET_ALL', conditions=dict(method=['GET']))
	dispatcher.connect('get_winner', '/bp/winners', controller=bpc, action='GET_WINNER', conditions=dict(method=['GET']))
	dispatcher.connect('get_year_winner', '/bp/winners/:year', controller=bpc, action='GET_YEAR_WINNER', conditions=dict(method=['GET']))
	dispatcher.connect('get_year', '/bp/year/:year', controller=bpc, action='GET_YEAR', conditions=dict(method=['GET']))
	dispatcher.connect('put_movie', '/bp/', controller=bpc, action='PUT_MOVIE', conditions=dict(method=['PUT']))
	dispatcher.connect('post_movie', '/bp/', controller=bpc, action='POST_MOVIE', conditions=dict(method=['POST']))
	dispatcher.connect('delete_movie', '/bp/:id', controller=bpc, action='DELETE_MOVIE', conditions=dict(method=['DELETE']))
	dispatcher.connect('delete_all', '/bp/', controller=bpc, action='DELETE_ALL', conditions=dict(method=['DELETE']))
	dispatcher.connect('reset', '/reset/', controller=bpc, action='RESET', conditions=dict(method=['PUT']))


	dispatcher.connect('bp_options', '/bp/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('bp_id_options', '/bp/:id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
	
	
	# Configuration
	conf = {
		'global' : {
				'server.socket_host': 'student12.cse.nd.edu',
				'server.socket_port': 51059,
		},
		'/' : {
			'request.dispatch' : dispatcher,
			'tools.CORS.on' : True,
		}
	}

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

class optionsController:
	def OPTIONS(self, *args, **kwargs):
		return ""

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
