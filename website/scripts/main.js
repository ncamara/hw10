console.log('Page loaded and entered main.js');

var submitBtn = document.getElementById("bsr-submit-button");
submitBtn.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('Entered getFormInfo function');
    var year = document.getElementById("year-text").value;
    makeNetworkCallToOscarsAPI(year);
}

function makeNetworkCallToOscarsAPI(year){
    console.log('entered network call to our API');
    // We are running this on student12
    var url = "http://student12.cse.nd.edu:51059/bp/year/" + year;
    console.log("URL: " + url);
    var xhr = new XMLHttpRequest(); 
    xhr.open("GET", url, true);

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        updateMovieWithResponse(year, xhr.responseText);
    }

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); // actually send req with no message body
}


function updateMovieWithResponse(year, response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var label1 = document.getElementById('response-line1');



    // There should be 
    if(response_json['movies'].length === 0){
        label1.innerHTML = 'Apologies, we could not find a movie from that year. The first Oscars were awarded in 1927, and the most recent data we have is from 2019.';
        var label2 = document.getElementById('response-line2');
        label2.innerHTML = ''
        return;
    } else{
        var moviesLen = response_json['movies'].length;
        var index = Math.floor(Math.random() * moviesLen);
        console.log(index);
        var movie = response_json['movies'][index];
        if(movie['winner'] === 'TRUE'){
            label1.innerHTML = 'You should watch Best Picture winner ' + movie['title'];
        }
        else{
            label1.innerHTML = 'You should watch Best Picture nominee ' + movie['title'];
        }
        // make nw call to breweries api
        makeNetworkCallToBreweriesApi(movie['id']);
    }

} // end of updateAgeWithResponse


function makeNetworkCallToBreweriesApi(movieID){
    console.log('Entered network call to breweries API');

    movieID = parseInt(movieID);
    // The IDs of the breweries start at 8000, but the movie IDs start at 1 so we add 8000
    movieID = movieID + 8033;


    var url = 'https://api.openbrewerydb.org/breweries/' + String(movieID);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true); 

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        updateBreweryWithResponse(xhr.responseText);
    }

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateBreweryWithResponse(response_text){
    // Get document element, create a string with the brewery name, and add to inner HTML
    console.log('entered update function')
    var label2 = document.getElementById("response-line2");
    var response_json = JSON.parse(response_text);

    var responseBrewery = response_json['name'];
    var display_text = 'You should drink a beer from ' + responseBrewery + ' while watching that movie.';
    label2.innerHTML = display_text;

    // Dynamically add label by appending it to the response-div
    /*label_item = document.createElement("label");
    label_item.setAttribute("id", "dynamic-label");

    var item_text = document.createTextNode(display_text);
    label_item.appendChild(item_text);

    var response_div = document.getElementById("response-div");
    response_div.appendChild(label_item);*/

}