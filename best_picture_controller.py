#!/usr/bin/python3

import cherrypy
import json
from best_picture_library import BestPictureLibrary

class BestPictureController:
	
	def __init__(self, bpl=None):
		if bpl is None:
			self.bpl = BestPictureLibrary()
		else:
			self.bpl = bpl

		self.bpl.load_movies('data.json')

	def RESET(self):
		output = {'result' : 'success'}
		self.bpl = self.bpl.load_movies('data.json')
		return json.dumps(output)
	
	def GET_ALL(self):
		output = {'result' : 'success'}

		if self.bpl.movies:
			output['movies'] = self.bpl.movies
		else:
			output['result'] = 'error'
			output['message'] = 'database is empty'

		return json.dumps(output)
	
	def GET_WINNER(self):
		output = {'result' : 'success'}

		output['movies'] = self.bpl.get_winners()

		return json.dumps(output)
	
	def GET_YEAR_WINNER(self, year):
		output = {'result' : 'success'}

		year_movies = self.bpl.get_year_winners(int(year))

		if year_movies:
			output['movies'] = year_movies
		else:
			output['result'] = 'error'
			output['message'] = 'the specified year was not found in the dataset'

		return json.dumps(output)

	def GET_YEAR(self, year):
		output = {'result' : 'success', 'movies' : []}

		year_movies = self.bpl.get_year(int(year))
		if year_movies:
			for movie in year_movies:
				output['movies'].append(movie)
		else:
			output['result'] = 'error'
			output['message'] = 'the specified year was not found in the dataset'

		return json.dumps(output)

	def PUT_MOVIE(self):
		output = {'result' : 'success'}
		data = json.loads(cherrypy.request.body.read())

		winner = None
		title = None
		id = data['id']
		year = data['year']

		if 'winner' in data.keys():
			winner = data['winner']
		if 'title' in data.keys():
			title = data['title']

		if self.bpl.edit_movie(id, year, winner, title):
			return json.dumps(output)
		else:
			output = {'result' : 'error'}
			output['message'] = 'we could not find a movie with that id'
		
		return json.dumps(output)

	def POST_MOVIE(self):
		output = {'result' : 'success'}

		data = json.loads(cherrypy.request.body.read())

		year = data['year']
		winner = data['winner']
		title = data['title']

		if self.bpl.add_movie(year, winner, title):
			return json.dumps(output)
		else:
			output['result'] = 'error'
			output['message'] = 'you cannot add a movie from before 2020' 
		return json.dumps(output)
	
	def DELETE_MOVIE(self, id):
		output = {'result' : 'success'}
		if self.bpl.delete_movie(id):
			return json.dumps(output)
		else:
			output['result'] = 'error'
			output['message'] = 'you cannot delete a movie from before 2020'
		return json.dumps(output)
	
	def DELETE_ALL(self):
		output = {'result' : 'success'}
		self.bpl.delete_all()
		return json.dumps(output)

	def RESET(self):
		output = {'result' : 'success'}

		data = json.loads(cherrypy.request.body.read().decode())

		self.bpl.__init__()
		self.bpl.load_movies('data.json')
		
		return json.dumps(output)


#bpc=BestPictureController()
#print(bpc.GET_YEAR_WINNER(2015))
#print(bpc.GET_YEAR(2015))
#print(bpc.DELETE_MOVIE(564))
